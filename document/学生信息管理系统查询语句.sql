--如果已有该数据库，则删除
DROP DATABASE IF EXISTS StudentScore;

-- 创建数据库
CREATE DATABASE StudentScore

ON PRIMARY
  (NAME   =Studat1,
   FILENAME ='e:\dataBase\Studat1.mdf',
   SIZE  =10MB,
MAXSIZE  =50MB,
   FILEGROWTH =5MB)
   
 LOG ON
  (NAME   =Stulog,
   FILENAME ='e:\dataBase\Stulog.ldf',
   SIZE  =5MB,
   MAXSIZE  =25MB,
   FILEGROWTH  =5%)


--创建表

--Student表
drop table if exists Student;
create table Student(
	Student_id		int			primary key,
	Student_name	varchar(20)	not null,
	Student_sex		char(2)		check (Student_sex='男' or  Student_sex='女') not null,	
	Student_age		int			null,
	Student_grade	int			null
)


--Class表

drop table if exists Class
create table Class(
	Class_id		int		primary key,
	Class_name		varchar(20)	not null,
	Class_count		int		null
)

--Score表

drop table if exists Score
create table Score(
	Student_id		int		not null,
	Class_id		int		not null,
	Student_score	int		null,

	/*定义两个外键*/
	foreign key(Student_id)	references Student(Student_id),
	foreign key(Class_id)   references Class(Class_id)
)


--创建学生选课成绩视图Student_score,
--以学生的ID和课程ID为索引，可以看到每个学生的学号、姓名、课程名称、课程ID、学生成绩、取得学分

drop view if exists Student_score 
SELECT	Score.Class_id, 
		Class.Class_name, 
		Student.Student_name, 
		Student.Student_id, 
		Score.Student_score, Class.Class_count
FROM (Class INNER JOIN Score 
ON Class.Class_id = Score.Class_id) 
INNER JOIN Student 
ON Score.Student_id = Student.Student_id;
