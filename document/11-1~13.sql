/*10-27*/
CREATE DATABASE Employee
 ON PRIMARY
  (NAME   =Empdat1,
   FILENAME ='D:\ShenZhen University\大三上\数据库原理与运用\数据库\data\Empdat1.mdf',
   SIZE  =10MB,
MAXSIZE  =50MB,
   FILEGROWTH =5MB)
   
 LOG ON
  (NAME   =Emplog,
   FILENAME ='D:\ShenZhen University\大三上\数据库原理与运用\数据库\data\Emplog.ldf',
   SIZE  =5MB,
   MAXSIZE  =25MB,
   FILEGROWTH  =5%)
ALTER DATABASE Employee
 ADD FILE
  (NAME  =Empdat2,
  FILENAME ='D:\ShenZhen University\大三上\数据库原理与运用\数据库\data\Empdat2',
  SIZE  =10MB,
  MAXSIZE  =25MB,
  FILEGROWTH =2%)
  
ALTER DATABASE Employee
 MODIFY FILE
  (NAME  =Empdat1,
  MAXSIZE  =80MB)
  
use Employee

GO

CREATE TABLE person
(
  P_no			char(6)		PRIMARY KEY,
  P_name		varchar(10) NOT NULL,
  Sex			char(2)		NOT NULL,
  Birthdate		datetime	NULL,
  Date_hired	datetime	NOT NULL,
  Deptname		varchar(10)	NOT NULL DEFAULT '培训部',
  P_boss		char(6)		NULL,
  CONSTRAINT birth_hire_check
  CHECK (Birthdate < Date_hired)
)

CREATE TABLE customer
(
 Cust_name		Char(6)		not null,
 Cust_no		Varchar(10) not null,
 Sex			Char(2)		not null,
 BirthDate		Datetime	null,
 City			VarChar(10)	null,
 
 Discount		Decimal(3,2)	not null		
)

CREATE TABLE orderdetail
(
 Order_no		char(6)		PRIMARY KEY
 constraint		Order_no_constraint
 check			(Order_no	like '[A-Z][A-Z][0-9][0-9][0-9][0-9]'),
 Cust_no		char(6)		not null,
 P_no			char(6)		not null,
 Order_total	int			not null,
 Order_date		datetime	not null)
  /*,constraint	person_contr
  foreign		key(P_no)
  references	person(P_no)
  on delete No Action
  on update cascade
  constraint	customer_contr
  foreign key(Cust_no)
  references	customer(Cust_no)
  on	delete		cascade
  on	update		cascade*/

create table salary
(
	P_no		char(6)		not null,
	Base		Decimal(5)	null,
	Bonus		Decimal(5)  null,
	Fact		As Base+Bonus,
)


/*11-3*/

create view CustomerView as
	select Cust_no,Cust_name,Sex,Discount
	from customer
	where City = '北京'

create view TrainingView as
	select person.P_no,P_name,Sex,Deptname,SUM(order_total) as Achievement
	from person,orderdetail
	where person.P_no = orderdetail.P_no and Deptname='培训部'
	and P_boss is not null and order_date >= GETDATE()-365

group by person.P_no , P_name,Sex,Deptname

SELECT * FROM [TrainingView]
SELECT * FROM [CustomerView]	


/*11-4*/
create index name_sort on person (P_name)

create index birth_name on person(Birthdate,P_name)

create unique index u_name_sort on person(P_name)

create clustered index fact_idx on salary(Fact DESC) /*簇族索引，DESC是降序排列，ASC是升序*/


/*11-5*/

drop INDEX salary.fact_idx

drop index person.u_name_sort
/*11-6*/

/*插入数据*/

insert into person
	values
	('000001','林峰','男','1973-04-07','2003-08-03','销售部','000007'),
	('000002','谢志文','男','1975-02-14','2003-12-07','培训部','000005'),
	('000003','李浩然','男','1970-08-25','2000-05-16','销售部','000003'),
	('000004','廖小玲','女','1979-08-06','2004-05-06','培训部','000005'),
	('000005','梁玉琼','女','1970-08-25','2001-03-13','培训部',NULL),
	('000006','罗向东','男','1979-05-11','2000-07-29','销售部','000007'),
	('000007','肖家庆','男','1963-07-14','1998-06-06','销售部',NULL),
	/*因为前面创建了人员列的唯一索引U_sort_name，所以这里不能有重复的人名，第二个李浩然只能改成李浩然1，否则无法插入。*/
	('000008','李浩然1','男','1975-01-30','2002-04-12','培训部','000005'),
	('000009','赵文龙','男','1969-04-20','1996-08-12','销售部','000007');

insert into salary (P_no,Base,Bonus)
	values
	('000001',2100,300),
	('000002',1800,300),
	('000003',2800,280),
	('000004',2500,250),
	('000005',2300,275),
	('000006',1750,130),
	('000007',2400,210),
	('000008',1800,235),
	('000009',2150,210);

insert into customer(Cust_no,Cust_name,Sex,BirthDate,City,Discount)
	values
	('000001','王云','男','1972-01-30','成都',1.00),
	('000002','林国平','男','1985-08-14','成都',0.85),
	('000003','郑洋','女','1973-04-07','成都',1.00),
	('000004','张雨洁','女','1983-09-06','北京',1.00),
	('000005','郑箐','女','1971-08-20','北京',0.95),
	('000006','李宇中','男','1979-08-06','上海',1.00),
	('000007','顾培铭','男','1973-07-23','上海',1.00);

insert into orderdetail
	values
	('AS0058','000006','000002',150000,'2006-04-05'),
	('AS0043','000005','000005',90000,'2006-03-25'),
	('AS0030','000003','000001',70000,'2006-02-14'),
	('AS0012','000002','000005',85000,'2005-11-11'),
	('AS0011','000007','000009',130000,'2005-08-13'),
	('AS0008','000001','000007',43000,'2005-06-06'),
	('AS0005','000001','000007',72000,'2005-05-12'),
	('BU0067','000007','000003',110000,'2005-03-08'),
	('BU0043','000004','000008',70000,'2004-12-25'),
	('BU0039','000002','000005',90000,'2004-10-12'),
	('BU0032','000006','000002',32000,'2004-08-08'),
	('BU0021','000004','000006',66000,'2004-04-01'),
	('CX0044','000007','000009',80000,'2003-12-12'),
	('CX0032','000003','000001',35000,'2003-09-18'),
	('CX0025','000002','000003',90000,'2003-05-02'),
	('CX0022','000001','000007',66000,'2002-12-04');

/*2021年12月10日08:49:23*/

/*11-7*/

/*给000006员工的工资涨到1800，奖金为160*/
update salary
set Base = 1800 ,Bonus = 160
where P_no = '000006'

/*给两年没有签订订单的员工的奖金下调25%*/
update salary
set Bonus = Bonus*0.75
where not exists(	select*from orderdetail
					where salary.P_no = orderdetail.P_no and
					Order_date >= getdate()-365*2)


/*11-8*/

/*删除person中的000010员工*/
delete from person where P_no = '000010'

/*11-9*/

/*把王云用户的购买折扣改为0.85*/
update CustomerView
set Discount = 0.85
where Cust_name='王云'

/*11-10*/

/*向视图插入数据*/
insert CustomerView (Cust_no,Cust_name,Sex,Discount)
	values
	('000008','刘美萍','女','1.00') 

/*select * from CustomerView*/

/*11-11*/

drop view CustomerView

/*11-12*/

select * from customer

/*11-13*/

/*1 查询person表中所有不重复的部门*/
select DISTINCT Deptname from person

/*2 查询person表里女经理的数据*/
select * from person
	where P_boss is null and Sex = '女'

/*3 查询person表里姓名为林峰、谢志文和罗向东的员工数据*/
select * from person
where P_name in('林峰','谢志文','罗向东')

/*4 利用SQL把03-08员工按实发工资升序排列*/
select * from salary
where P_no between '000003'and'000008'
order by Fact ASC

/*5 查询person员工号为000002，基本工资增加两倍，奖金1.5倍后的实际收入 */
select P_no 工号,2*base + 1.5*bonus 实际收入 from salary
where	P_no = '000002'

/*11-14*/

/*利用SQL语句查询一月份发放奖金平均数大于200的部门，并从高到低排序(降序)*/

select Deptname 部门 from salary
A JOIN person B ON A.P_no = B.P_no
group by Deptname
having AVG(Bonus)>200
order by AVG(Bonus) DESC

/*查询居住城市在上海的顾客订单总数和订单总额*/

select count(*) 订单总数,sum(Order_total) 订单总额
from orderdetail,customer
where orderdetail.Cust_no=customer.Cust_no and City='上海'

/*11-15*/

/*特殊联结查询，
	查询培训部员工签订订单的情况
	查询工作时间比他们的部门经理还长的员工
	查询至少有两份订单的顾客信息
	查询姓名相同的员工信息*/

select p.P_no,COUNT(*) 订单总数,sum(Order_total) 订单总额
from orderdetail o,person p
where o.P_no = p.P_no and Deptname='培训部'
group by p.P_no

select p.P_no,p.P_name
from person p,person m
where p.P_boss = m.P_no and p.Date_hired <m.Date_hired

select distinct c.Cust_no,Cust_name,Sex,Discount from customer c,orderdetail o1,orderdetail o2
	where c.Cust_no = o1.Cust_no and c.Cust_no = o2.Cust_no
		and o1.Order_no<>o2.Order_no

select p1.P_no ,p1.P_name,p1.Sex,p1.Deptname
from person p1,person p2
where p1.P_name = p2.P_name and p1.P_no<>p2.P_no
/*11-16*/

/*嵌套子查询
	查询比工号为000005的员工实发工资高的所有员工信息
	查询订单额高于平均订单额的订单信息
	查询与成都的顾客签订订单的员工代码及姓名
	查询一年内没有续订单的顾客信息*/

select p.P_no 员工号,p_name 姓名,Fact 实发
from person p ,salary s
where p.P_no = s.P_no
	and s.Fact > (select Fact from salary
				  where P_no='000005')

select order_no,Cust_name,P_name,Order_total,Order_date
from orderdetail o,person p, customer c
where o.Cust_no = c.Cust_no and o.P_no = p.P_no and 
Order_total>(select AVG(Order_total)from orderdetail)

select distinct p.P_no,P_name
from orderdetail o,person p
where p.P_no = o.P_no and
	Cust_no in (select Cust_no from customer where City='成都')

select  Cust_no,Cust_name,Sex,Discount
from customer
where Cust_no not in (select distinct Cust_no
						from orderdetail
						where Order_date >=GETDATE()-365)
/*11-17*/

/*相关子查询
	查询至少有两份订单的顾客信息
	查询至少有一份订单额大于100000或总订单额大于200000的顾客信息*/


/* 使用UNION查询
	利用SQL语句分别查询居住城市在上海、北京的顾客信息，合并输出*/


select distinct c.Cust_no,Cust_name,Sex,Discount
from customer c,orderdetail o1
where c.Cust_no = o1.Cust_no and o1.Cust_no in (select Cust_no
from orderdetail o2
where o1.Order_no <>o2.Order_no)


/*11-27*/

create rule Discount_rule as @ Discount between 0.50 and 1.00

sp_bindrule 'Discount_rule','customer.Discount'

create rule Sex_rule as @ Sex in('男','女')

sp_bindrule 'Sex_rule','person.Sex'

sp_unbindrule 'person.Sex'

drop rule Sex_rule

/*11-28*/
alter table salary with nocheck

add constraint Bonus_check CHECK(Bonus>=50)

exec sp_help salary

alter table salary

drop constraint Bonus_check

/*11-30*/

alter table person

add constraint unique_pon_pname unique(P_no,P_name)

/*11-31*/

alter table salary

add constraint deptno_FK foreign key(deptno)

	references person(P_no)


/*11-32*/

insert person
	values('000012','宋全礼','男','1980-7-17','2005-3-11','培训部','000005')

update person set P_no='000016' where P_no='000003'

delete person where P_no='000001'


/*11-33*/

update person set P_no='000010' where exists
	(select *from orderdetail o where Order_no='cx2222' and person.P_no = o.P_no)




Create Database StuData
          On Primary      
               ( Name       = StuFile1,
                 Filename   = 'D:\ShenZhen University\大三上\数据库原理与运用\数据库\data\StuFile1.mdf', 
                 Size       = 10MB,
                 MaxSize    = 1000MB,
                 FileGrowth = 10MB),
              ( Name       = StuFile2,
                Filename   = 'D:\ShenZhen University\大三上\数据库原理与运用\数据库\data\StuFile2.ndf', 
                Size       = 10MB,
                MaxSize    = 1000MB,
                FileGrowth = 10%)
          Log On
              ( Name       = Stulog,
                Filename   = 'D:\ShenZhen University\大三上\数据库原理与运用\数据库\data\Stulog.ldf',
                Size       = 10MB,
                MaxSize    = 1000MB,
                FileGrowth = 10MB) 

 create table Studentdata
(
	Student_id		int			primary key,
	Student_name	varchar(10)	not null,
	Sex				char(2)		not null default '未知',
	Age				real		not null,
	Grade			int			not null,
)
